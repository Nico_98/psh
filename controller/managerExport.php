<?php
require("../model/connection.php");
require("../model/stadistic.php");

global $conn;
$stadistic = new stadistic(null,null,null,null,null);
$result=$stadistic->get($conn);
$var = "Puesto,Usuario,Puntos,Fecha en que los Obtuvo\n";
$puesto = 0;
foreach ($result as $reg) {
	$var.= $puesto.",".$reg->nickname.",".$reg->score.",".$reg->date_creation."\n";
	$puesto++;
}

header("Content-Description: File Transfer");
header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename=Top10_PSH-Game.csv");
echo $var;

?>