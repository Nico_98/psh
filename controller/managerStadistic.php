<?php
require("../model/connection.php");
require("../model/stadistic.php");

global $conn;

$nickname=array();
$player_image=array();
$score=array();
$date_creation=array();

$rs = new stadistic(null,null,null,null,null);
$rsa = $rs->get($conn);

foreach ($rsa as $key) {
   array_push($nickname,$key->nickname);
   array_push($player_image,$key->player_image);
   array_push($score,$key->score);
   array_push($date_creation,$key->date_creation);
}

$msg="ok";
print json_encode(array("msg"=>$msg,"nick"=>$nickname,"image"=>$player_image,"score"=>$score,"date"=>$date_creation));
?>