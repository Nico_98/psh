<?php
class stadistic{

	var $id;
	var $player_id;
	var $nickname;
	var $player_image;
	var $score;

	function __construct( $player_id,$nickname, $player_image, $score){

		$this->player_id = $player_id;
		$this->nickname = $nickname;
		$this->player_image = $player_image;
		$this->score = $score;
	}

	function get($conn) {
		$query = "SELECT * FROM estadistic_game ORDER BY score DESC LIMIT 10";
		$rs = $conn->query($query);
		$rs_fetch = $rs->fetchAll(PDO::FETCH_OBJ);
		return 	$rs_fetch;
	}

	function getCount($conn) {
		global $conn;
		$filter = '';
		
		$filter .= (isset($this->player_id) ? ($filter == ''?' WHERE ':' AND '). "  player_id = '$this->player_id'" : "");
		$filter .= (isset($this->nickname) ? ($filter == ''?' WHERE ':' AND '). "  nickname = '$this->nickname'" : "");
		$filter .= (isset($this->score) ? ($filter == ''?' WHERE ':' AND '). "  score = $this->score" : "");

		$query = "SELECT COUNT(*) AS total FROM estadistic_game  ".$filter;
		$rs = $conn->query($query);
		$rs_fetch = $rs->fetchAll(PDO::FETCH_OBJ);
		return 	$rs_fetch[0];
	}

	function set($conn) {
		global $conn;
		$conn->query("UPDATE score = $this->score, nickname = '$this->nickname', player_image='$this->player_image' WHERE estadistic_id = $this->id AND player_id=$this->player_id");
	}

	function add( $conn) {
		$conn->query("INSERT INTO estadistic_game (player_id,score,nickname,player_image) VALUES ($this->player_id,$this->score,'$this->nickname','$this->player_image')");
	}
}
?>