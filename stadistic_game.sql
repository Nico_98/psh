CREATE TABLE `estadistic_game` (
	`estadistic_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`player_id` INT(11) NOT NULL,
	`nickname` VARCHAR(120) NOT NULL COLLATE 'utf8mb4_general_ci',
	`player_image` VARCHAR(120) NOT NULL COLLATE 'utf8mb4_general_ci',
	`date_creation` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
	`score` INT(11) NOT NULL,
	PRIMARY KEY (`estadistic_id`) USING BTREE,
	UNIQUE INDEX `estadistic_id` (`estadistic_id`) USING BTREE
)